def solve(a):
  max1, max2 = 0, 0
  for item in a:
    if item > max1:
      max2 = max1
      max1 = item
    elif (item > max2):
      max2 = item
  return max1 + max2

def getTest():
  return solve([1,5, 0, 7, 2, 3, 46, 3, 15, 17, 4, 89, 12, 34, 43, 76, 2, 1, 23])

print(getTest())
