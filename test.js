const addDigits = (n) => {
	let re = 0;
	while (n > 0) {
		temp = n % 10;
		re += temp;
		n = (n - temp) / 10;
	}
	return re;
}


const addDigits3 = (n) => {
	let re = 0;
	while (n > 0) {
		temp = n % 10;
		re += temp;
		n = Math.floor(n/10);
	}
	return re;
}
const addDigits2 = (n) => {
	return n.toString().split('')
	.map(x => parseInt(x))
	.reduce((acc, c) => acc + c, 0);
}

const x = 4529;
console.log(`The result of digit summation of ${x} = ${addDigits(x)}`);
console.log(`The result of digit summation of ${x} = ${addDigits2(x)}`);
console.log(`The result of digit summation of ${x} = ${addDigits3(x)}`);

