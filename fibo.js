const _ = require('underscore');


const fibonacci = _.memoize((n) => {
	return n < 2 ? n : fibonacci(n - 1) + fibonacci(n - 2);
});

const x = 1000;
console.log(`The result of fibonacci(${x}) is : ${fibonacci(x)}`);
